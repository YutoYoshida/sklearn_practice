import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score

#アヤメデータの読み込み
iris_data = pd.read_csv("iris.csv",encoding="utf-8")

#ラベルと入力データに分離
y = iris_data.loc[:,"Name"]
x = iris_data.loc[:,["SepalLength","SepalWidth","PetalLength","PetalWidth"]]

#print(y)
#print(x)

#学習用とテスト用に分離
x_train, x_test, y_train, y_test = train_test_split(x,y,test_size=0.2,train_size=0.8,shuffle=True)

#学習
clf = SVC(max_iter=100000)
clf.fit(x_train,y_train)

#評価する
y_pred = clf.predict(x_test)
print("正解率->",accuracy_score(y_test,y_pred))
