import urllib.request as req
import pandas as pd

url = "https://raw.githubusercontent.com/pandas-dev/pandas/master/pandas/tests/data/iris.csv"
savefile = "iris.csv"
req.urlretrieve(url,savefile)
print("保存完了")

csv = pd.read_csv(savefile,encoding="utf-8")
print(csv)