#CSVファイルを１行ずつ読み込む
with open("weather.csv","rt",encoding="Shift_JIS") as fr:
	lines = fr.readlines()
	
#データセットのラベル付けを再定義する．
lines = ["年,月,日,気温,品質,均質\n"] + lines[5:]
lines = map(lambda v: v.replace('/',','),lines)
result = "".join(lines).strip()
print(result)

#再定義したデータセットを格納したCSVファイルを作成する.
with open("weather10y.csv","wt",encoding="utf-8") as fw:
	fw.write(result)
	print("created")
