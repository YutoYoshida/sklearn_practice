#ライブラリ
from sklearn.svm import LinearSVC
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report


#学習用データセットの準備
#ANDゲート
learn_data = [[0,0],[1,0],[0,1],[1,1]] #データセット
learn_label = [0,0,0,1] #正解ラベル

#学習アルゴリズムを選択
clf = LinearSVC(max_iter=100000)

#学習用データセットと結果の学習
clf.fit(learn_data,learn_label)

#テストデータによる予測
test_data = [[0,0],[1,0],[0,1],[1,1]]
test_label = clf.predict(test_data)

#学習データの評価
print(str(test_data) + "の予測結果" + str(test_label))
print("正答率->" + str(accuracy_score([0,0,0,1],test_label)))

print(classification_report(learn_label, test_label))
