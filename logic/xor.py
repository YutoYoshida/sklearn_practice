from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report

#学習用データ
#XORゲート
learn_data = [[0,0],[1,0],[0,1],[1,1]]
learn_label = [0,1,1,0]

#学習アルゴリズム
clf = KNeighborsClassifier(n_neighbors=1)

clf.fit(learn_data,learn_label)

test_data = [[0,0],[1,0],[0,1],[1,1]]
test_label = clf.predict(test_data)

print(str(test_data) + "の予測結果" + str(test_label))
print("正答率->" + str(accuracy_score([0,1,1,0],test_label)))

print(classification_report(learn_label, test_label))